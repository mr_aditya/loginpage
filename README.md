This is a ASP.NET Login Project

Architecture-

    Login.aspx is startup file
    
    Login.aspx runs script in Login.jsx file residing in Client/src folder
    
    Login.jsx script validates input fields and enable Login Button
    
    Id and Password are verified on Login.aspx.cs server side file. It redirects to Dashboard if Id and Password matches. Otherwise it shows error message.
    
    Dashboard.aspx is "logged in view" and have Logout button