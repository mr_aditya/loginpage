using System;
using System.Data.SqlClient;

namespace WebApplication5
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            InvLabel.Visible = false;
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            using (SqlConnection sqlCon = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=LoginDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"))
            {
                try { 
                    sqlCon.Open();
                    string query = "SELECT COUNT(1) FROM LoginTable WHERE Id=@id AND Password=@password";
                    SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                    sqlCmd.Parameters.AddWithValue("@id", IdText.Value.Trim());
                    sqlCmd.Parameters.AddWithValue("@password", PasswordText.Value.Trim());
                    int count = Convert.ToInt32(sqlCmd.ExecuteScalar());
                    if (count == 1)
                    {
                        Session["username"] = IdText.Value.Trim();
                        Response.Redirect("Dashboard.aspx");
                    }
                    else { InvLabel.Visible = true; }
                }
                catch (Exception ex)
                {
                    InvLabel.Visible = true;
                    InvLabel.Text = Convert.ToString(ex);
                }
                finally
                {
                    sqlCon.Close();
                }
            }
        }
    }
}