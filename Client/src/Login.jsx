var myInput = document.getElementById("PasswordText");
var myInput1 = document.getElementById("IdText");
var letter = document.getElementById("letter");
var capital = document.getElementById("capital");
var number = document.getElementById("number");
var length = document.getElementById("length");
var btn = document.getElementById("Button");
var l1 = document.getElementById("Label1");
var l2 = document.getElementById("Label2");

myInput1.onkeyup = function () {
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(myInput1.value)) {
        l1.innerHTML = 'Enter Valid Id';
    } else {
        l1.innerHTML = '';
    }
}
myInput.onkeyup = function () {
    var lowerCaseLetters = /[a-z]/g;
    var upperCaseLetters = /[A-Z]/g;
    var spec = /[!\\@\\#]/;
    var numbers = /[0-9]/g;
    if (myInput.value.match(lowerCaseLetters) && myInput.value.match(upperCaseLetters) && myInput.value.match(numbers) && (myInput.value.length >= 8) && myInput.value.match(spec)) {
        btn.disabled = '';
        l2.innerHTML = '';
    } else {
        btn.disabled = 'disabled';
        l2.innerHTML = 'Password must contain atleast 1 uppercase,1 lowercase,1 number,1 special character and minimum 8 characters';
    }
}