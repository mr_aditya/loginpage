<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebApplication5.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="Client/src/Style.css">

</head>
<body>
    <form runat="server">
        <p>
            <asp:Label runat="server" Text="Id"></asp:Label>
            <input ID="IdText" runat="server" type="text" />
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        </p>
        <p>
            <asp:Label runat="server" Text="Password"></asp:Label>
            <input ID="PasswordText" runat="server" type="password" />
            <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
        </p>
        <p>
            <asp:Button ID="Button" runat="server" OnClick="btnLogin_Click" Text="Button" Width="215px" disabled=disabled />
        </p>
        <p>
            <asp:Label ID="InvLabel" runat="server" Text="Invalid Id or Password!!!"></asp:Label>
        </p>
    </form>
<div id="message">
    <script src ="Client/src/Login.jsx">
    </script>
</body>
</html>
